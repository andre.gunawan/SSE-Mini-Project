from sanic import Sanic
from sanic.response import json
from orjson import dumps

from tinydb import Query
from aiotinydb import AIOTinyDB

app = Sanic("sse-server-app", dumps=dumps)

@app.get("/")
async def home(request):
    return json({
        'msg': 'Welcome to SSE Server'
    })

@app.post("/message")
async def service_add_message(request):
    data = request.json
    async with AIOTinyDB("db.json") as db:
        db.insert(data)

        return json({
            'status': 1,
            'msg': "Added Successfully"
        })

@app.post("/s/message")
async def service_search_message(request):
    data = request.json
    async with AIOTinyDB("db.json") as db:
        msg = Query()
        result = db.search(msg.tags.any(data['search_token']))
        return json(result)
    
