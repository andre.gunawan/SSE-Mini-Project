import click
from Crypto.Cipher import AES
from Crypto.Hash import SHA256

import base64
import requests
import random
import string
import uuid
import os

import nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

from tinydb import TinyDB, Query

db = TinyDB('db.json')

DEFAULT_URL = 'http://127.0.0.1:8000'

@click.group()
def command_list():
    pass

# ==========================================================================================
# ------------------------------------ Helper Function -------------------------------------
# ==========================================================================================

def get_random_string(length):
    result_str = ''.join(random.choice(string.ascii_letters) for i in range(length))
    return result_str

def init_key():
    key = get_random_string(16)
    with open('key', 'w') as f:
        f.write(key)

def check_key():
    cwd = os.getcwd()
    if os.path.isfile(cwd + '/key'):
        with open('key', 'r') as f:
            content = f.read()
            if content == '':
                return False
            else:
                return True
    else:
        return False

def encrypt_msg(msg):
    key = ''
    with open('key', 'r') as f:
        key = f.read()
    
    key_byte = bytes(key, 'utf-8')
    msg_byte = bytes(msg, 'utf-8')
    
    cipher = AES.new(key_byte, AES.MODE_EAX)
    nonce = cipher.nonce
    ciphertext, tag = cipher.encrypt_and_digest(msg_byte)
    return ciphertext, nonce, tag

def decrypt_msg(encrypted_msg, nonce):
    key = ''
    with open('key', 'r') as f:
        key = f.read()
    
    key_byte = bytes(key, 'utf-8')
    
    cipher = AES.new(key_byte, AES.MODE_EAX, nonce=nonce)
    plaintext = cipher.decrypt(encrypted_msg)
    return plaintext

def process_msg(msg):

    ps = PorterStemmer()
    stopWords = set(stopwords.words('english'))


    tokens = nltk.word_tokenize(msg)
    word_list = []
    for w in tokens:
        if w not in stopWords:
            # w = ps.stem(w)
            if w not in string.punctuation and w != "n't":
                word_list.append(w.strip().lower())
    return word_list

# ==========================================================================================
# -------------------------------------- Command List --------------------------------------
# ==========================================================================================

@click.command()
def keyinfo():
    """Check Current Key"""
    if check_key():
        with open('key', 'r') as f:
            key = f.read()
            click.echo('Current Key: {}'.format(key))
    else:
        click.echo('Key Not Found ... Starting to generate Key ...')
        init_key()


@click.command()
@click.option("--sender", prompt="From: ", help="The one who send the message")
@click.option("--to", prompt="To: ", help="The one who get the message")
@click.option("--msg", prompt="Message: ", help="The actual message")
def send(sender, to, msg):

    # Formating  the message
    content = ""
    content += "From: {}\n".format(sender)
    content += "To: {}\n\n".format(to)
    content += msg
    
    # Processing the message to get all the word
    word_list = process_msg(msg)

    # Add Header to word_list
    print(sender)
    print(to)
    word_list.append(nltk.word_tokenize(sender.strip().lower())[0])
    word_list.append(nltk.word_tokenize(to.strip().lower())[0])

    # Create Word Tokens
    tokens = []
    for word in word_list:
        h = SHA256.new()
        h.update(bytes(word, 'utf-8'))
        token = h.hexdigest()
        print("{} -> {}".format(word, token))
        tokens.append(token)

    # Encrypt the Whole Content
    encrypted_msg, nonce, tag = encrypt_msg(content)
    enc_msg_str = base64.b64encode(encrypted_msg).decode('utf-8')
    nonce_str = base64.b64encode(nonce).decode('utf-8')

    # Creating Doc ID
    doc_id = str(uuid.uuid4())

    # Save nonce in local database
    data = {
        'doc_id': doc_id,
        'nonce': nonce_str
    }
    db.insert(data)

    # Format Document
    doc = {
        'doc_id': doc_id,
        'tags': tokens,
        'msg': enc_msg_str
    }

    # Send Data to Server
    r = requests.post(DEFAULT_URL + '/message', json=doc)
    click.echo(str(r.json()))

@click.command()
@click.option("--query", prompt="Search Word: ", help="Search Message in Server, Words are seperated using single space")
def search(query):
    # Parse Every Word in Query
    words = nltk.word_tokenize(query)

    # Create Search Tokens
    tokens = []
    for word in words:
        h = SHA256.new()
        h.update(bytes(word.strip().lower(), 'utf-8'))
        token = h.hexdigest()
        tokens.append(token)

    # Create a Search Request
    query = {
        'search_token': tokens
    }

    # Send Search Request to Server
    r = requests.post(DEFAULT_URL + '/s/message', json=query)
    results = r.json()

    # Decrypt Search Results
    click.echo('========================================================================')
    for doc in results:
        Doc = Query()
        query_result = db.search(Doc.doc_id == doc['doc_id'])[0]
        nonce = base64.b64decode(query_result['nonce'])
        msg = base64.b64decode(doc['msg'])

        plaintext = decrypt_msg(msg, nonce)
        click.echo(plaintext.decode('utf-8') + "\n\n")
        


command_list.add_command(keyinfo)
command_list.add_command(send)
command_list.add_command(search)

if __name__ == '__main__':
    if not check_key():
        init_key()
    command_list()